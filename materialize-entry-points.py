#!/usr/bin/env python
"""load a meta.yaml and create scripts for each entry point


    $> misc/materialize-entry-points.py recipe/meta.yaml build/scripts

"""
import os
import os.path
import sys
import stat

import yaml

import toolbox.fileutils as fu
import toolbox.cli.api as cli


def create_unix(module, name):
    return f"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import {module}
if __name__ == '__main__':
    sys.exit({module}.{name}() or 0)
""".lstrip()


def main(options=None):
    options = parse_args(options) if isinstance(options, (list, tuple, None.__class__)) else options

    with open(options.metafile, 'rb') as fp:
        data = yaml.load(fp)

    entrypoints = data.get('build', {}).get('entry_points', [])
    for entry in entrypoints:
        name, _, rest = entry.partition('=')
        if ':' in rest:
            module, _, fn = rest.rpartition(':')
        else:
            module, fn = rest, 'main'
        script = create_unix(module.strip(), fn.strip())
        dst = fu.npath(options.outputdir, name.strip())
        with open(dst, 'wb') as fp:
            fp.write(script.encode('utf-8'))
        if fu.iswin():
            mode = stat.S_IREAD
        else:
            mode = stat.S_IXUSR | stat.S_IRGRP | stat.S_IXGRP | stat.S_IXOTH | stat.S_IROTH
        os.chmod(dst, os.stat(dst).st_mode | mode)


def add_arguments(p):
    p.add_argument('metafile', help='input meta.yaml file')
    p.add_argument('outputdir', help='output dir to generate the scripts')


def process_options(o):
    if not fu.exists(o.metafile):
        o.error('missing {0}'.format(o.metafile))
    if fu.exists(o.outputdir) and os.path.isfile(o.outputdir):
        o.error('output is a file {0}'.format(o.outputdir))
    o.outputdir = fu.makedirs(o.outputdir)


def parse_args(args=None):
    parser = cli.ArgumentParser(doc=__doc__)
    add_arguments(parser)
    options = parser.parse_args(args)
    options = process_options(options) or options
    return options

if __name__ == '__main__':
    sys.exit(main() or 0)
